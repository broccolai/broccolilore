package co.uk.magmo.broccolilore.Commands;

import static co.uk.magmo.broccolilore.Utilities.Messenger.playerMessage;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.List;

public class Clear {
    public static void clear(CommandSender sender) {
        Player player = (Player) sender;
        ItemStack itemStack = player.getInventory().getItemInMainHand();
        ItemMeta itemMeta = itemStack.getItemMeta();

        if (itemMeta.hasLore()) {
            List<String> lores = itemMeta.getLore();

            lores.remove(lores.size() - 1);

            itemMeta.setLore(lores);
            itemStack.setItemMeta(itemMeta);

            playerMessage(player, "Item lore line removed", false);
        } else {
            playerMessage(player, "This item does not have any lore", true);
        }
    }

    public static void clearAll(CommandSender sender) {
        Player player = (Player) sender;
        ItemStack itemStack = player.getInventory().getItemInMainHand();
        ItemMeta itemMeta = itemStack.getItemMeta();

        if (itemMeta.hasLore()) {
            List<String> lores = itemMeta.getLore();
            lores.clear();

            itemMeta.setLore(lores);
            itemStack.setItemMeta(itemMeta);

            playerMessage(player, "All item lore line removed", false);
        } else {
            playerMessage(player, "This item does not have any lore", true);
        }
    }
}
