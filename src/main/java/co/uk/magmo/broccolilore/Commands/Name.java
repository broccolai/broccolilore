package co.uk.magmo.broccolilore.Commands;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import static co.uk.magmo.broccolilore.Utilities.Messenger.playerMessage;

public class Name {
    public static void name(CommandSender sender, String[] args) {
        // Setting item name

        Player player = (Player) sender;
        ItemStack itemStack = player.getInventory().getItemInMainHand();
        ItemMeta itemMeta = itemStack.getItemMeta();
        StringBuilder buildName = new StringBuilder();

        for (int i = 1; i < args.length; i++)
            buildName.append(args[i]).append(" ");

        String name = buildName.toString();

        name = name.replace("&", "§");

        itemMeta.setDisplayName(name);
        itemStack.setItemMeta(itemMeta);

        playerMessage(player, "Item successfully named", false);
    }
}
