package co.uk.magmo.broccolilore;

import static co.uk.magmo.broccolilore.Utilities.Messenger.consoleMessage;

import co.uk.magmo.broccolilore.Utilities.GlowEnchant;
import co.uk.magmo.broccolilore.Utilities.TabCompletion;
import org.bukkit.NamespacedKey;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.plugin.java.JavaPlugin;

import java.lang.reflect.Field;

public class BroccoliLore extends JavaPlugin {

    @Override
    public void onEnable() {
        registerGlow();

        consoleMessage("Lore Plugin enabled");

        this.getCommand("Lore").setExecutor(new CommandDispatcher());
        this.getCommand("Lore").setTabCompleter(new TabCompletion());
    }

    @Override
    public void onDisable() {
        consoleMessage("Lore Plugin disabled");
    }

    private void registerGlow() {
        try {
            Field f = Enchantment.class.getDeclaredField("acceptingNew");
            f.setAccessible(true);
            f.set(null, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            GlowEnchant glow = new GlowEnchant(NamespacedKey.minecraft("53254"));
            Enchantment.registerEnchantment(glow);
        } catch (IllegalArgumentException e) {
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
