package co.uk.magmo.broccolilore.Utilities;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TabCompletion implements TabCompleter {
    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] arguments) {
        if (command.getName().equalsIgnoreCase("lore")) {
            if (arguments.length == 1) {
                List<String> list = new ArrayList<>();

                list.add("Add");
                list.add("Name");
                list.add("Clear");
                list.add("ClearAll");
                list.add("Glow");

                Collections.sort(list);

                return list;
            }
        }

        return null;
    }
}
